package JavaProgramming;

import javax.xml.stream.events.Characters;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DuplicateCharInString {

    String s = "SushmaAhirwar";

    public void findDuplicate(String s)
    {
        char[] chars = s.toCharArray();

        Map<Character,Integer> charMap = new HashMap<Character,Integer>();

        for(Character ch : chars)
        {
            if(charMap.containsKey(ch))
            {
              charMap.put(ch,charMap.get(ch)+1)  ;
            }
            else
                charMap.put(ch,1);
        }

        Set<Character> keys = charMap.keySet();

        for(Character ch : keys)
        {
            if(charMap.get(ch)> 1)
            {
                System.out.println(ch+"==="+charMap.get(ch));
            }
        }

    }

    public static void main(String args[])
    {

        DuplicateCharInString obj = new DuplicateCharInString();
        obj.findDuplicate(obj.s);
    }
}
