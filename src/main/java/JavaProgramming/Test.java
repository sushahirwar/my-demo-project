package JavaProgramming;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Test {
    String str = "SushmaAhirwar";

    Integer[] dup = {1, 2, 1, 4, 3, 2, 5, 1, 3, 2, 4, 5, 6, 6, 7, 8, 9, 0};

    public void duplicateString(String s) {
        str = s.toLowerCase();

        char[] ch = str.toCharArray();

        Map<Character, Integer> hm = new HashMap<>();

        for (Character chars : ch) {
            if (hm.get(chars) == null) {
                hm.put(chars, 1);
            } else {
                hm.put(chars, hm.get(chars) + 1);
            }
        }

        Set<Map.Entry<Character, Integer>> sm = hm.entrySet();

        for (Map.Entry<Character, Integer> m : sm) {
            if (m.getValue() > 1) {
                System.out.println(m.getKey() + " " + m.getValue());
            }
        }

    }

    public void duplicateInteger(Integer[] sn) {
        Map<Integer, Integer> hm = new HashMap<>();

        for (Integer in : sn) {
            if (hm.get(in) == null) {
                hm.put(in, 1);
            } else {
                hm.put(in, hm.get(in) + 1);
            }

        }

        Set<Map.Entry<Integer, Integer>> ss = hm.entrySet();

        for (Map.Entry<Integer, Integer> s : ss) {
            if (s.getValue() > 1) {
                System.out.println(s.getKey() + " " + s.getValue());
            }

        }

    }

    public void anagramTest() {
        String s1 = "aabbcc BCA";
        String s2 = "bbccaa acbd";
        s1 = s1.replace(" ", "").toLowerCase();
        s2 = s2.toLowerCase().replace(" ", "");

        StringBuilder ns = new StringBuilder();
        ns.append(s2);

        char[] ch = s1.toCharArray();

        for (Character chars : ch) {
            int index = ns.indexOf("" + chars);
            if (index != -1) {

                ns.deleteCharAt(index);
            }
        }

        if (ns.length() == 0) {
            System.out.println("String is anagram");
        } else {
            System.out.println("Not Anagram");
        }
    }

    public void fibbonaciSeries() {
        int n = 10;
        int fnum = 0;
        int snum = 1;
        int tnum = 0;
        System.out.print(fnum + ",");
        System.out.print(snum + ",");
        for (int i = 2; i < n; i++) {
            tnum = fnum + snum;
            System.out.print(tnum + ",");
            fnum = snum;
            snum = tnum;
        }
    }

    public void factorialSeries() {
        int n = 4;
        int result = 1;
        for (int i = n; i >= 1; i--) {
            result = i * result;
        }
        System.out.println(result);
    }

    public void compressStringResult() {
        String ss = "aaaabbcccddd"; //result = a4b2c3d3
        //  String result = "";

        char[] ch = ss.toCharArray();

        Map<Character, Integer> hm = new HashMap<>();

        for (Character chars : ch) {
            if (hm.get(chars) == null) {
                hm.put(chars, 1);
            } else {
                hm.put(chars, hm.get(chars) + 1);
            }
        }
        Set<Map.Entry<Character, Integer>> sm = hm.entrySet();

        for (Map.Entry<Character, Integer> m : sm) {

            System.out.print(m.getKey() + "" + m.getValue());
        }
    }


    public void primeNumber() {
        int num = 11;
        int count = 0;
        for (int i = 2; i < num / 2; i++) {
            if (num % i == 0) {
                count = count + 1;
            } else {
                count = count;
            }
        }

        if (count > 0) {
            System.out.println("Number not prime");
        } else
            System.out.println("Number is prime");
    }

    public static void main(String args[]) {
        Test obj = new Test();
        //    obj.duplicateString(obj.str);
        //   obj.duplicateInteger(obj.dup);
        //  obj.anagramTest();
        //  obj.fibbonaciSeries();

        //  obj.factorialSeries();

          obj.compressStringResult();
        //obj.primeNumber();

    }
}
