package JavaProgramming;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DuplicateStringFind {

    public static void main (String args[])
    {
        String s = "This is my place to work,i am writing duplicate character find program";
        s = s.toLowerCase().replace(" ","");

        char[] ch = s.toCharArray();

        Map<Character,Integer> hm = new HashMap<>();

        for(Character chars : ch)
        {
            Integer count = hm.get(chars);
            if(count == null)
            {
                hm.put(chars,1);
            }
            else
            {
                count = count+1;
                hm.put(chars,count);
            }
        }

        Set<Map.Entry<Character,Integer>> es = hm.entrySet();

        for (Map.Entry<Character,Integer> as : es)
        {
            if(as.getValue()>1)
            {
                System.out.println(as.getKey()+" "+as.getValue());
            }
        }



    }
}
