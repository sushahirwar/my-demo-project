package JavaProgramming;

public class AnagramProgram {

    String s1 = "aabbcc";
    String s2 = "bbccaaa";

    public boolean checkAnagram(String a1,String a2)
    {
        char [] c1 = a1.toCharArray();

        StringBuilder secondString = new StringBuilder(a2);

        for (Character ch : c1)
        {
            int index = secondString.indexOf(""+ch);
            //if index will not found ,we will get -1
            if(index != -1)
            {
                secondString.deleteCharAt(index);
            }
            else
            {
              System.out.println();
            }
        }

        if(secondString.length() == 0)
            return true;
        else
            return false;

    }

    public static void main(String args[])
    {
        AnagramProgram obj = new AnagramProgram();

       boolean returnData = obj.checkAnagram(obj.s1,obj.s2);
        System.out.println("Anagram="+returnData);

    }


}
