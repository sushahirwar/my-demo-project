package JavaProgramming;

public class CompressString {
    String s = "aaabbccd";
    //output should be a3b2c2d1

    public void compressString(String s) {
        int count = 0;
        Character temp = s.charAt(0);
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == temp) {
                count++;
            } else {
                System.out.print(temp + "" + count);
                count = 1;
                temp = s.charAt(i);
            }
        }
        System.out.print(temp + "" + count);
    }

    public static void main(String args[]) {
        CompressString obj = new CompressString();
        obj.compressString(obj.s);
    }


}
