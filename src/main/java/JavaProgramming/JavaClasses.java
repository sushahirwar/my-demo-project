package JavaProgramming;

public class JavaClasses {

  //  public  static float radius = 10f;


    public static void main(String args[])
    {
        Circle c1= new Circle(10f);
     //   c1.radius=10f;
        System.out.println("Area of circle: " +c1.area());

        Circle c2= new Circle(10f);
        System.out.println("Area of circle: " +c2.area());
       // c2.radius= 15f;
        System.out.println("circumference of circle: " +c2.circumference());


        Square s1 = new Square(10f);
     //   s1.length = 10f;
        Square s2 = new Square(20f);
     //   s2.length =20f;
        System.out.println("Area of square: " +s1.area());
        System.out.println("Area of square: " +s2.area());

    }


}

class Circle{
    public  final float radius ;
    public static float PI = 3.1414f;

    public  float area()
    {
        return PI * this.radius * this.radius;
    }

    public Circle(float radius)
    {
        this.radius = radius;
    }

    public float circumference()
    {
        return 2 * PI * this.radius;
    }

}


class Square{

    public float length;

    public float area()
    {
        return  length * length;
    }

    public Square(float len)
    {
        length= len;
    }


}
