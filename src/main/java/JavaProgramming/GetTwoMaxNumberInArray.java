package JavaProgramming;

public class GetTwoMaxNumberInArray {


    public static void main(String[] args) {
        Integer[] arr = {8, 4, 2, 14, 17, 68, 99};
        int firstmax = 0;
        int secondmax = 0;

        for (int i = 0; i <arr.length; i++) {
            int num = arr[i];
            if (num > firstmax) {
                secondmax = firstmax;
                firstmax = arr[i];
            } else if (num > secondmax) {
                secondmax = num;
            }
        }
        System.out.println("First max Number =" + firstmax);
        System.out.println("Second max Number =" + secondmax);


    }

}
