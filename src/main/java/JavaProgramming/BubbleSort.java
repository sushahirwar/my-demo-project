package JavaProgramming;

public class BubbleSort {

    public static void main(String[] args) {
        Integer[] arr = {2, 6, 3, 5, 7, 9, 3, 0, 18, 4, 66};

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                int temp = 0;

                if (arr[i] > arr[j]) {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;

                }
            }
            System.out.print(arr[i] + ",");
        }

    }

}
