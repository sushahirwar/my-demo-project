package JavaProgramming;

public class SelectionSort {

    public static void main(String[] args) {
        Integer[] arr = {2, 6, 3, 5, 7, 9, 3, 0, 18, 4, 66};

        for (int i = 0; i < arr.length; i++) {
            int index = arr[i];
            for (int j = i + 1; j < arr.length; j++) {
                int temp = 0;

                if (index > arr[j]) {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                    index = arr[i];
                }
            }
            System.out.print(arr[i] + ",");
        }
    }
}
